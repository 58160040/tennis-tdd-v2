// function start(){
//     return 'Love-Love'
// }
// function addScore(){
//     return 'Fifteen - Love'
// }
function TennisGame(){
    this.playerAScore
    this.playerBScore

    function checkScore(score){
        if (score === 0){
            return 'Love'
        }else if (score === 15){
            return 'Fifteen'
        }else if (score === 30){
            return 'Thirty'
        }else if (score === 40){
            return 'Forty'
        }
    }
    this.start = () =>{
        this.playerAScore = 0
        this.playerBScore = 0
    }
    this.echoScore = () => {
        if(this.playerBScore >= 40 && this.playerAScore >= 40 && this.playerBScore > this.playerAScore){
            return 'PlayerB adventage'
        }
        else if(this.playerAScore >= 40 && this.playerBScore >= 40 && this.playerAScore > this.playerBScore){
            return 'PlayerA adventage'
        }else if (this.playerBScore > 40 && this.playerAScore !== 40){
            return 'PlayerB wins game'
        }
        else if(this.playerAScore > 40 && this.playerBScore !== 40 )
        {
            return 'PlayerA wins game'
        }
        else if (this.playerAScore === 40 && this.playerBScore === 40){
            return 'Deuce'
        }
        else{
            var playerAScoreString = checkScore(this.playerAScore)
            var playerBScoreString = checkScore(this.playerBScore)
            var scoreString = playerAScoreString + ' - ' + playerBScoreString
            return scoreString
        }
        
    }
    this.playerAGetScore = () =>{
        if(this.playerAScore === 30){
            this.playerAScore +=10
        }else{
            this.playerAScore += 15
        }
    }
    
    this.playerBGetScore = () =>{
        if(this.playerBScore === 30){
            this.playerBScore +=10
        }else{
            this.playerBScore += 15
        }
    }
}

var tennisGame

function startGame() {
    tennisGame = new TennisGame()
    tennisGame.start()
}
function addScoreplayerA(count){
    for (let i = 0 ; i < count ; i++)
    {
        tennisGame.playerAGetScore()
    }
}
function addScoreplayerB(count){
    for (let i = 0 ; i < count ; i++)
    {
        tennisGame.playerBGetScore()
    }
}
test('"Love - Love" when game started ',()=>{
    startGame()
    var scoreString = tennisGame.echoScore()
    
    expect(scoreString).toBe('Love - Love')
})
test('"Fifteen - Love" when A get first scored',()=>{
    startGame()
    addScoreplayerA(1)
    var scoreString = tennisGame.echoScore()
    expect(scoreString).toBe('Fifteen - Love')
})

test('"Thirty - Love" when A get double scored',()=>{
    startGame()
    
    addScoreplayerA(2)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Thirty - Love')
})

test('"Forty - Love" when A get triple scored',()=>{
    startGame()
    addScoreplayerA(3)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Forty - Love')
})
test('"PlayerA wins game when A get 4 score before B"',()=>{
    startGame()
    addScoreplayerA(4)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA wins game')
})


test('"Love - Fifteen" when A get first scored',()=>{
    startGame()
    addScoreplayerB(1)
    var scoreString = tennisGame.echoScore()
    expect(scoreString).toBe('Love - Fifteen')
})
test('"Love - Thirty" when A get double scored',()=>{
    startGame()
    
    addScoreplayerB(2)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Love - Thirty')
})

test('"Love - Forty" when A get triple scored',()=>{
    startGame()
    addScoreplayerB(3)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Love - Forty')
})
test('"PlayerB wins game when A get 4 score before A"',()=>{
    startGame()
    addScoreplayerB(4)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerB wins game')
})

test('"Deuce" Score when score is 40-40',()=>{
    startGame()
    addScoreplayerA(3)
    addScoreplayerB(3)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Deuce')
})

test('"PlayerA adventage"when A get score after deuce', () =>{
    startGame()
    addScoreplayerA(4)
    addScoreplayerB(3)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA adventage')
})
test('"PlayerB adventage"when B get score after deuce', () =>{
    startGame()
    addScoreplayerA(3)
    addScoreplayerB(4)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerB adventage')
})